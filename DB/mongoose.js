const mongoose = require("mongoose");
mongoose.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    dbName: process.env.DB_NAME,
}).then(() => {
    console.log("connected to mongo DB");
}).catch((e) => {
    console.log("error in connect to db", e.message);
});
