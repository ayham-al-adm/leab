const router = require("express").Router();
const searchController = require("../controllers/search.controller");

router.get("/:page", searchController.searchGet);
router.get("/", searchController.searchGet);
router.post("/", searchController.searchPost);
module.exports = router;
