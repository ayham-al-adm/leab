const router = require("express").Router();
const adminController = require("../controllers/admin.controller");
const User = require("../models/user.model");
const AdminAuth = require("../middlewares/admin.auth");
const Guest = require("../middlewares/guest");
const UploadFile = require("../middlewares/upload-file");
const UploadFiles = require("../middlewares/upload-multiple-files");

router.post("/login", Guest, adminController.postLogin);
router.get("/login", Guest, adminController.getLogin);

router.get("/category/add", AdminAuth, adminController.getAddCategory);
router.post("/category/add", AdminAuth, UploadFile, adminController.postAddCategory);
router.get("/category", AdminAuth, adminController.getCategories);

router.get("/product", AdminAuth, adminController.getProducts);
router.get("/product/add", AdminAuth, adminController.getAddProduct);
router.post("/product/add", AdminAuth, UploadFiles, adminController.postAddProduct);

router.get("/create-test", async (req, res) => {
    try {
        await User.create({ email: "a2@a.a", password: "$2a$10$/uW/ITWt.POKRQRaFQj9LuyWbvDaHV0cN/tlFFer9maqvmN3qZOG." });
        res.json("done");
    } catch (error) {
        res.json(error.message);
    }
});
router.get("/", AdminAuth, adminController.getHome);

module.exports = router;
