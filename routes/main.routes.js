const router = require("express").Router();
const postsController = require("../controllers/posts.controller");

router.get("/offers", postsController.getOffers);
router.get("/:page", postsController.getAllPosts);
router.get("/", postsController.getAllPosts);
module.exports = router;
