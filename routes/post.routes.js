const router = require("express").Router();
const postsController = require("../controllers/posts.controller");

router.get("/delete-and-post-test", postsController.test);
router.get("/:id", postsController.getPostDetails);
router.get("/", postsController.getAllPosts);
module.exports = router;
