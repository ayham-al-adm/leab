window.onload = () => {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.get("status")) {
        document.getElementById("status").value = urlParams.get("status");
        document.getElementById("sort").value = urlParams.get("sort");
    }
};

/* eslint-disable no-unused-vars */
const paginationClickHandler = (page) => {
    window.location = `/search?page=${page}&status=${document.getElementById("status").value}&sort=${document.getElementById("sort").value}`;
};

const submitButton = document.getElementById("submit-button");
submitButton.onclick = (e) => {
    e.preventDefault();
    window.location = `/search?page=1&status=${document.getElementById("status").value}&sort=${document.getElementById("sort").value}`;
};

const toggleCaret = document.getElementById("caret-toggle");
toggleCaret.onclick = () => {
    if (toggleCaret.getAttribute("data-toggle") === "open") {
        document.getElementById("search-form").classList.add("closed-search-form");
        toggleCaret.classList.remove("fa-caret-up");
        toggleCaret.classList.add("fa-caret-down");
        toggleCaret.setAttribute("data-toggle", "close");
    } else {
        document.getElementById("search-form").classList.remove("closed-search-form");
        toggleCaret.classList.remove("fa-caret-down");
        toggleCaret.classList.add("fa-caret-up");
        toggleCaret.setAttribute("data-toggle", "open");
    }
};
