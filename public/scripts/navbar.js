window.addEventListener("load", () => {
    document.getElementById("main-nav").classList.remove("scrolled-navbar");
});

document.onscroll = () => {
    if (window.pageYOffset < 100) {
        document.getElementById("main-nav").classList.remove("scrolled-navbar");
    } else {
        document.getElementById("main-nav").classList.add("scrolled-navbar");
    }
};
