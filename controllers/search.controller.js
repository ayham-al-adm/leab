const Post = require("../models/post.model");

const extractSortObject = async (sort) => {
    if (sort === "price-asc") {
        return { "mainPrice.value": 1 };
    }
    if (sort === "price-desc") {
        return { "mainPrice.value": -1 };
    }
    if (sort === "date-asc") {
        return { date: 1 };
    }
    if (sort === "date-desc") {
        return { date: -1 };
    }
    return {};
};

module.exports.searchGet = async (req, res) => {
    const page = req.query.page ?? 1;
    const count = 20;
    const offset = (page - 1) * count;

    const products = await Post.find({ status: req.query.status })
        .sort(await extractSortObject(req.query.sort))
        .limit(count).skip(offset);
    const productsCount = await Post.find({ status: req.query.status }).countDocuments();
    return res.render("search/index.ejs", {
        products,
        status: req.query.status,
        sort: req.query.sort,
        page,
        pages: Math.ceil(productsCount / count),
    });
};

module.exports.searchPost = async (req, res) => {
    const page = req.params.page ?? 1;
    const count = 20;
    const offset = (page - 1) * count;

    const products = await Post.find({ status: req.body.status })
        .sort(await extractSortObject(req.body.sort))
        .limit(count).skip(offset);
    const productsCount = await Post.find({ status: req.body.status }).countDocuments();
    return res.render("search/index.ejs", {
        products,
        status: req.body.status,
        sort: req.body.sort,
        page,
        pages: Math.ceil(productsCount / count),
    });
};
