const bcrypt = require("bcryptjs");
// const axios = require("axios");
const imgur = require("imgur");
const User = require("../models/user.model");
const Category = require("../models/category.model");
const Post = require("../models/post.model");

module.exports.getLogin = async (req, res) => res.render("admin/auth/login");

module.exports.postLogin = async (req, res) => {
    const user = await User.findOne({ email: req.body.email });
    if (!user) {
        // return with error
        return res.redirect("/admin/login");
    }
    const isMatched = bcrypt.compareSync(req.body.password, user.password);
    if (!isMatched) {
        // return with error
        return res.redirect("/admin/login");
    }
    req.session.isLoggedIn = true;
    req.session.user = user;
    await req.session.save();
    return res.redirect("/admin");
};

module.exports.getHome = async (req, res) => {
    const users = await User.find();
    res.render("admin/home", { users, path: "/admin" });
};

module.exports.getCategories = async (req, res) => {
    const categories = await Category.find();
    res.render("admin/categories/index", { categories, path: "/admin/category" });
};

module.exports.getAddCategory = async (req, res) => {
    res.render("admin/categories/add", { path: "/admin/category/add" });
};

module.exports.postAddCategory = async (req, res) => {
    try {
        // let imageLink = null;
        // const response = await axios.post("https://api.imgur.com/3/image",
        //     req.file,
        //     {
        //         headers: {
        //             Authorization: "Client-ID 41be3aabc31e224",
        //         },
        //     });
        // const { link } = response.data.data;
        // imageLink = link;
        // console.log(imageLink);
        const result = await imgur.uploadFile(`./public/images/${req.file.filename}`);
        await Category.create({
            descriptionAr: req.body.descriptionAr,
            descriptionEn: req.body.descriptionEn,
            image: result.link,
            baseCategory: req.body.baseCategory !== "" ? req.body.baseCategory : null,
        });
        res.redirect("/admin/category");
    } catch (err) {
        console.log(err.message, err);
        res.render("admin/categories/add", { path: "/admin/category/add" });
    }
};

module.exports.getProducts = async (req, res) => {
    const products = await Post.find();
    res.render("admin/products/index", { products, path: "/admin/product" });
};

module.exports.getAddProduct = async (req, res) => {
    res.render("admin/products/add", { path: "/admin/product/add" });
};
async function sentFilesIntoImgur(req) {
    let filePaths = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const file of req.files) {
        filePaths.push(`./public/images/${file.filename}`);
    }
    const result = await imgur.uploadImages(filePaths, "File");
    filePaths = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const file of result) {
        filePaths.push(file.link);
    }
    return filePaths;
}

async function extractProperties(text) {
    const slicedProps = text.split("\n");
    const props = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const prop of slicedProps) {
        const propData = prop.split(":");
        props.push({
            name: propData[0],
            value: propData[1],
        });
    }
    return props;
}

async function saveNewProduct(req, links) {
    const props = await extractProperties(req.body.properties);
    if (req.body.alternativePrice) {
        await Post.create({
            images: links,
            status: req.body.status,
            description: req.body.description,
            isOffer: true,
            mainPrice: {
                amount: req.body.mainPrice,
                currency: req.body.mainCurrency,
            },
            alternativePrice: {
                amount: req.body.alternativePrice,
                currency: req.body.alternativeCurrency,
            },
            country: req.body.country,
            city: req.body.city,
            addrerss: req.body.addrerss,
            phone: req.body.phone,
            category: req.body.category,
            date: new Date(),
            youtubeUrl: req.body.youtubeUrl,
            properties: props,
        });
    } else {
        await Post.create({
            images: links,
            description: req.body.description,
            status: req.body.status,
            isOffer: false,
            mainPrice: {
                amount: req.body.mainPrice,
                currency: req.body.mainCurrency,
            },
            country: req.body.country,
            city: req.body.city,
            addrerss: req.body.addrerss,
            phone: req.body.phone,
            category: req.body.category,
            date: new Date(),
            youtubeUrl: req.body.youtubeUrl,
            properties: props,
        });
    }
}

module.exports.postAddProduct = async (req, res) => {
    try {
        const links = await sentFilesIntoImgur(req);
        await saveNewProduct(req, links);
        res.redirect("/admin/product");
    } catch (err) {
        console.log(err.message, err);
        res.render("admin/products/add", { path: "/admin/product/add" });
    }
};
