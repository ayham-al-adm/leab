const Post = require("../models/post.model");

module.exports.addPost = async (req, res) => {
    const post = await Post.create({
        images: ["image1", "image2", "image3"],
        description: "منتج رقم 1",
        properties: [{
            name: "cpu",
            value: "Core i7 8800HQ",
        }, {
            name: "RAM",
            value: "16 GB",
        }],
        mainPrice: {
            amount: "5000000",
            currency: "ليرة سورية",
        },
        alternativePrice: {
            amount: "5400000",
            currency: "ليرة سورية",
        },
    });

    return res.status(201).json(post);
};

module.exports.getAllPosts = async (req, res) => {
    const count = 20;
    let offset = 0;
    let page = req.params.page ? +req.params.page : 1;
    page = page < 1 ? 1 : page;
    offset = (page - 1) * count;

    const posts = await Post.find().skip(offset).limit(count).sort({ date: -1 });
    const totalPostsCount = await Post.find().countDocuments();
    res.status(200).render("main.ejs", { posts, page, pages: Math.ceil(totalPostsCount / count) });
    // res.status(200).json(posts);
};

module.exports.getPostDetails = async (req, res) => {
    const post = await Post.findOne({ _id: req.params.id });
    res.status(200).render("posts/index.ejs", { post });
};

module.exports.test = async (req, res) => {
    // await Post.remove();
    const post = await Post.create({
        images: ["laptop.jpg", "laptop.jpg", "laptop.jpg"],
        isOffer: true,
        description: "لابتوب الألعاب الأقوى والأفضل من جهة الأداء - مناسب لعشاق الألعاب و البرامج الهندسية الضخمة وبطارية عالمية وبنظام تبريد يفوق الخيال ب4 بطاريات ",
        properties: [{
            name: "المعالج",
            value: "Core i7 7700HQ",
        }, {
            name: "الرام",
            value: "16 GB",
        }, {
            name: "كرت الشاشة",
            value: "منفصل Nvidia 1440 RTX",
        }, {
            name: "الهارد",
            value: "500GB SSD M2 - 1TB HDD",
        }, {
            name: "الشاشة",
            value: "15.6 Full HD",
        }, {
            name: "البطارية",
            value: "20AMH 6 ساعات",
        }],
        mainPrice: {
            amount: "5000000",
            currency: "ليرة سورية",
        },
        alternativePrice: {
            amount: "5400000",
            currency: "ليرة سورية",
        },
        country: "سوريا",
        city: "دمشق",
        address: "البحصة - خلف البرج الذهبي",
        phone: "099999999",
        // categry: "",
    });

    return res.status(201).json(post);
};

module.exports.getOffers = async (req, res) => {
    const count = 20;
    let offset = 0;
    let page = req.params.page ? +req.params.page : 1;
    page = page < 1 ? 1 : page;
    offset = (page - 1) * count;

    const posts = await Post.find({ isOffer: true }).skip(offset).limit(count).sort({ date: -1 });
    const totalPostsCount = await Post.find().countDocuments();
    res.status(200).render("posts/offers.ejs", { posts, page, pages: Math.ceil(totalPostsCount / count) });
    // res.status(200).json(posts);
};
