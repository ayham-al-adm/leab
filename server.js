const app = require("./app");
const port = process.env.PORT || 3000;
require("./DB/mongoose");

app.listen(port, () => {
    console.log("Server started at port:", port);
});
