const mongoose = require("mongoose");

const schema = mongoose.Schema({
    id: mongoose.ObjectId,
    email: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model("User", schema);
