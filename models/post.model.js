const mongoose = require("mongoose");

const schema = mongoose.Schema({
    id: mongoose.ObjectId,
    images: [String],
    description: String,
    isOffer: Boolean,
    status: String,
    mainPrice: {
        amount: String,
        currency: String,
    },
    alternativePrice: {
        amount: String,
        currency: String,
    },
    properties: [{
        name: String,
        value: String,
    }],
    country: String,
    city: String,
    address: String,
    phone: String,
    category: {
        type: mongoose.ObjectId,
        ref: "Category",
    },
    date: Date,
    youtubeUrl: String,
});

module.exports = mongoose.model("Post", schema);
