const mongoose = require("mongoose");

const schema = mongoose.Schema({
    id: mongoose.ObjectId,
    image: String,
    descriptionAr: String,
    descriptionEn: String,
    baseCategory: {
        type: mongoose.ObjectId,
        ref: "Category",
    },
});

module.exports = mongoose.model("Category", schema);
