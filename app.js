const express = require("express");
const app = express();
const path = require("path");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const mainRoutes = require("./routes/main.routes");
const postsRoutes = require("./routes/post.routes");
const adminRoutes = require("./routes/admin.routes");
const searchRoutes = require("./routes/search.routes");
const User = require("./models/user.model");
app.set("view engine", "ejs");
app.set("views", "views");
app.use(express.static(path.join(__dirname, "public")));
app.use("/public/images", express.static(path.join(__dirname, "public", "images")));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const store = new MongoDBStore({
    uri: process.env.DB_URL,
    collection: "sessions",
    databaseName: process.env.DB_NAME,
});

app.use(session({
    secret: "secret sessions",
    resave: true,
    saveUninitialized: true,
    strore: store,
}));

app.use(async (req, res, next) => {
    if (!req.session.user) {
        return next();
    }
    const user = await User.findById(req.session.user._id);
    req.user = user;
    return next();
});
app.use("/search", searchRoutes);
app.use("/post", postsRoutes);
app.use("/admin", adminRoutes);
app.use("/", mainRoutes);
module.exports = app;
